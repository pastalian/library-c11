#include <assert.h>
#include <memory.h>
#include <stddef.h>
#include <stdlib.h>

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

// stretchy buffer
typedef struct BufHdr {
    size_t len;
    size_t cap;
    char buf[0];
} BufHdr;

#define buf__hdr(b) ((BufHdr *)((char *)(b) - offsetof(BufHdr, buf)))
#define buf__fits(b, n) (buf_len(b) + (n) <= buf_cap(b))
#define buf__fit(b, n) (buf__fits(b, n) ? 0 : ((b) = buf__grow(b, buf_len(b) + (n), sizeof(*(b)))))
#define buf_len(b) ((b) ? buf__hdr(b)->len : 0)
#define buf_cap(b) ((b) ? buf__hdr(b)->cap : 0)
#define buf_push(b, x) (buf__fit(b, 1), (b)[buf_len(b)] = (x), buf__hdr(b)->len++)
#define buf_free(b) ((b) ? free(buf__hdr(b)) : 0)

void *buf__grow(const void *buf, size_t new_len, size_t elem_size) {
    size_t new_cap = MAX(1 + 2 * buf_cap(buf), new_len);
    size_t new_size = offsetof(BufHdr, buf) + new_cap * elem_size;
    BufHdr *new_hdr;
    if (buf) {
        new_hdr = realloc(buf__hdr(buf), new_size);
    } else {
        new_hdr = malloc(new_size);
        new_hdr->len = 0;
    }
    new_hdr->cap = new_cap;
    return new_hdr->buf;
}

typedef struct InternStr {
    size_t len;
    const char *str;
} InternStr;

static InternStr *interns;

const char *str_intern_range(const char *start, const char *end) {
    size_t len = end - start;
    for(size_t i = 0; i < buf_len(interns); ++i) {
        if(interns[i].len == len && strncmp(interns[i].str, start, len) == 0) {
            return interns[i].str;
        }
    }
    char *str = malloc(len + 1);
    memcpy(str, start, len);
    str[len] = 0;
    buf_push(interns, (InternStr){len, str});
    return str;
}

const char *str_intern(const char *str) {
    return str_intern_range(str, str + strlen(str));
}

void str_intern_test(){
    char *x = "hello";
    char *y = "hello";
    assert(x != y);
    const char *x_intern = str_intern(x);
    const char *y_intern = str_intern(y);
    assert(x_intern == y_intern);
    char *z = "hello!";
    const char *z_intern = str_intern(z);
    assert(z_intern != x_intern);
}

int main(int argc, char **argv){
    str_intern_test();
    return 0;
}
