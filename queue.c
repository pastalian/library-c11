#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MIN(x, y) ((x) <= (y) ? (x) : (y))
typedef struct QueueKey {
    char str[16];
    int num;
} QueueKey;
typedef struct QueueDat {
    QueueKey key;
    struct QueueDat *next;
} QueueDat;
typedef struct Queue {
    size_t len;
    QueueDat *head;
    QueueDat *tail;
} Queue;
#define queue_init(q) ((q) ? ((q)->len = 0, (q)->head = (q)->tail = NULL) : 0)
#define queue_len(q) ((q) ? (q)->len : 0)
void queue_push(Queue *q, QueueKey x) {
    QueueDat *new_dat = malloc(sizeof(QueueDat));
    *new_dat = (QueueDat){x, NULL};
    if(q->len) {
        q->tail->next = new_dat;
        q->tail = new_dat;
    }else{
        q->head = q->tail = new_dat;
    }
    q->len++;
}
QueueKey queue_pop(Queue *q) {
    assert(q->len);
    QueueDat *old_head = q->head;
    q->head = old_head->next;
    QueueKey key = old_head->key;
    free(old_head);
    q->len--;
    return key;
}
void queue_test() {
    Queue Q;
    queue_init(&Q);
    for(int i = 0; i < 10; ++i) {
        QueueKey key;
        queue_push(&Q, key);
    }
    while(queue_len(&Q)) {
        queue_pop(&Q);
    }
}
int main(int argc, char **argv) {
    queue_test();
    return 0;
}
