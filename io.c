#include <stdio.h>

#define arr_print_d0dn(a, n) for(int i=0;i<(n)-1;++i)put_uint((a)[i]),putchar_unlocked(' ');put_uint((a)[(n)-1]),putchar_unlocked('\n')
#define arr_rprint_d0dn(a, n) for(int i=(n)-1;i>0;--i)put_uint((a)[i]),putchar_unlocked(' ');put_uint(*(a)),putchar_unlocked('\n')
#define arr_print_dndn(a, n) for(int i=0;i<(n);++i)put_uint((a)[i]),putchar_unlocked('\n')
#define arr_rprint_dndn(a, n) for(int i=(n)-1;i>=0;--i)put_uint((a)[i]),putchar_unlocked('\n')

void put_uint(int n) {
    if(!n) {
        putchar_unlocked('0');
        return;
    }
    char buf[11];
    int i = 0;
    while(n) buf[i++] = (char)(n % 10 + '0'), n /= 10;
    while(i--)putchar_unlocked(buf[i]);
}
void put_int(int n) {
    if(!n) {
        putchar_unlocked('0');
        return;
    }
    if(n < 0) n = -n, putchar_unlocked('-');
    char buf[11];
    int i = 0;
    while(n) buf[i++] = (char)(n % 10 + '0'), n /= 10;
    while(i--)putchar_unlocked(buf[i]);
}
void put_str(char *str) { while(*str) putchar_unlocked(*str++); }

int get_uint() {
    int n = 0;
    int c = getchar_unlocked();
    if(c < 48 || 57 < c) return c;
    while(47 < c && c < 58) n = 10 * n + (c & 0xf), c = getchar_unlocked();
    return n;
}
int get_int() {
    int n = 0;
    int c = getchar_unlocked();
    if(c == 45) {
        c = getchar_unlocked();
        while(47 < c && c < 58) n = 10 * n + (c & 0xf), c = getchar_unlocked();
        return -n;
    } else if(c < 48 || 57 < c) return c;
    while(47 < c && c < 58) n = 10 * n + (c & 0xf), c = getchar_unlocked();
    return n;
}
int get_str(char *str) {
    int c;
    while((c = getchar_unlocked()) > 32) *str++ = (char)c;
    *str = 0;
    return c;
}
int get_line(char *str) {
    int c;
    while((c = getchar_unlocked()) > 31) *str++ = (char)c;
    *str = 0;
    return c;
}

#define INPUT__LENGTH (20000000)
char _in[INPUT__LENGTH];
char *_i = _in;
// fread_unlocked(_in, 1, INPUT__LENGTH, stdin);
int read_uint() {
    int n = 0;
    while(47 < *_i && *_i < 58) n = 10 * n + (*_i++ & 0xf);
    _i++;
    return n;
}
#define OUTPUT__LENGTH (20000000)
char _out[OUTPUT__LENGTH];
char *_o = _out;
// fwrite_unlocked(_out, 1, _o - _out, stdout);
void write_uint(int n) {
    if(!n) {
        *_o++ = '0';
        return;
    }
    char buf[11];
    int i = 0;
    while(n) buf[i++] = (n % 10 + '0'), n /= 10;
    while(i--) *_o++ = buf[i];
}
