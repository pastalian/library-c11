#include <ctype.h>
#include <memory.h>
#include <stddef.h>
#include <stdlib.h>

#define MAX(x, y) ((x) >= (y) ? (x) : (y))

// stretchy buffer
typedef struct BufHdr {
    size_t len;
    size_t cap;
    char buf[0];
} BufHdr;

#define buf__hdr(b) ((BufHdr *)((char *)(b) - offsetof(BufHdr, buf)))
#define buf__fits(b, n) (buf_len(b) + (n) <= buf_cap(b))
#define buf__fit(b, n) (buf__fits((b), (n)) ? 0 : ((b) = buf__grow((b), buf_len(b) + (n), sizeof(*(b)))))
#define buf_len(b) ((b) ? buf__hdr(b)->len : 0)
#define buf_cap(b) ((b) ? buf__hdr(b)->cap : 0)
#define buf_push(b, ...) (buf__fit((b), 1), (b)[buf__hdr(b)->len++] = (__VA_ARGS__))
#define buf_free(b) ((b) ? (free(buf__hdr(b)), (b) == NULL) : 0)

void *buf__grow(const void *buf, size_t new_len, size_t elem_size) {
    size_t new_cap = MAX(1 + 2 * buf_cap(buf), new_len);
    size_t new_size = offsetof(BufHdr, buf) + new_cap * elem_size;
    BufHdr *new_hdr;
    if(buf) {
        new_hdr = realloc(buf__hdr(buf), new_size);
    } else {
        new_hdr = malloc(new_size);
        new_hdr->len = 0;
    }
    new_hdr->cap = new_cap;
    return new_hdr->buf;
}

typedef struct InternStr {
    size_t len;
    const char *str;
} InternStr;

static InternStr *interns;

const char *str_intern_range(const char *start, const char *end) {
    size_t len = end - start;
    for(size_t i = 0; i < buf_len(interns); ++i) {
        if(interns[i].len == len && strncmp(interns[i].str, start, len) == 0) {
            return interns[i].str;
        }
    }
    char *str = malloc(len + 1);
    memcpy(str, start, len);
    str[len] = 0;
    buf_push(interns, (InternStr){len, str});
    return str;
}

const char *str_intern(const char *str) {
    return str_intern_range(str, str + strlen(str));
}

typedef enum TokenKind {
    TOKEN_LAST_CHAR = 127, // Reserve first 128 values for one-char tokens
    TOKEN_INT,
    TOKEN_NAME,
} TokenKind;

typedef struct Token {
    TokenKind kind;
    const char *start;
    const char *end;
    union {
        int val;
        const char *name;
    };
} Token;

Token token;
const char *stream;

void next_token() {
    token.start = stream;
    if(isdigit(*stream)) {
        int val = 0;
        while(isdigit(*stream)) {
            val *= 10;
            val += *stream++ - '0';
        }
        token.kind = TOKEN_INT;
        token.val = val;
    } else if(isalpha(*stream) || *stream == '_') {
        while(isalnum(*stream) || *stream == '_') {
            stream++;
        }
        token.kind = TOKEN_NAME;
        token.name = str_intern_range(token.start, stream);
    } else {
        token.kind = *stream++;
    }
    token.end = stream;
}

void lex_test() {
    char *source = "12*12hello!";
    stream = source;
    next_token();
    while(token.kind) {
        next_token();
    }
}

int main(int argc, char **argv) {
    lex_test();
    return 0;
}
